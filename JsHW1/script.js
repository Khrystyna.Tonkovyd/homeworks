(function init() {

    const notAllowedMessage = "You are not allowed to visit this website.";
    const areYouSureMessage = "Are you sure you want to continue?";
    const yourNameMessage = "Your name:";
    const welcomeMessage = "Welcome,";

    function showWelcomeMessage(name) {
        alert(`${welcomeMessage} ${name}`);
    }

    function requestVisitorName() {
        let name = prompt(`${yourNameMessage}`);
        if (!name) {
            return requestVisitorName();
        } else {
            return name;
        }
    }

    function requestVisitorAge() {
        let age = prompt("Your age, only numbers!:");
        if (isNaN(age)) {
            return requestVisitorAge();
        }
        if (!age) {
            return requestVisitorAge();
        } else {
            return age;
        }
    }

    function requestSiteEntranceConfirmation(name) {
        if (confirm(areYouSureMessage)) {
            showWelcomeMessage(name);
        } else showNotAllowedMessage();
    }

    function showNotAllowedMessage() {
        alert(notAllowedMessage);
        close();
    }

    function validateAge(age, name) {
        if (age < 18) {
            showNotAllowedMessage();
        }
        if (age >= 18 && age <= 22) {
            requestSiteEntranceConfirmation(name);
        }
        if (age >= 23) {
            showWelcomeMessage(name);
        }
    }

    let visitorName = requestVisitorName();
    let visitorAge = requestVisitorAge(visitorName);
    validateAge(visitorAge, visitorName);

})();
