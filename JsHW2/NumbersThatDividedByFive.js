(function init() {

    const divisor = 5;

    function showAlertNoNumbers() {
        alert(`Sorry, no numbers :(`);
    }

    function requestIntegerNumber() {
        let number = prompt(`Entered your number, only integer! : `);
        let parseInteger = parseInt(number);
        if (number != parseInteger)
            return requestIntegerNumber();
        else
            return parseInteger;
    }

    function findTheNearestNumberThatIsDividedByDivisor(number, divisor) {
        while ((number % divisor) != 0)
            number--;
        return number;
    }

    function printEachNumberThatDividedByDivisor(number, divisor) {
        while (number > 0) {
            console.log(number);
            number -= divisor;
        }
    }

    let number = requestIntegerNumber();
    if (number >= 0 && number < divisor)
        return showAlertNoNumbers();

    let numberThatDividedByDivisor = findTheNearestNumberThatIsDividedByDivisor(number, divisor);
    printEachNumberThatDividedByDivisor(numberThatDividedByDivisor, divisor);

})();
